package service

import (
	"context"

	gateMod "gitlab.com/timofeev.pavel.art/rpc_project-gateway_api/pkg/models"
	"gitlab.com/timofeev.pavel.art/rpc_project-gateway_api/pkg/processing/exchange"

	"gitlab.com/timofeev.pavel.art/rpc_project-exchange_api/internal/infrastructure/errors"

	"gitlab.com/timofeev.pavel.art/rpc_project-exchange_api/internal/models"
	"gitlab.com/timofeev.pavel.art/rpc_project-exchange_api/internal/storages"
	"go.uber.org/zap"
)

type ExchangeService struct {
	storage storages.Storages
	logger  *zap.Logger
}

func NewExchangeService(storage storages.Storages, logger *zap.Logger) *ExchangeService {
	return &ExchangeService{
		storage: storage,
		logger:  logger,
	}
}

func (e *ExchangeService) Upsert(ctx context.Context, in CryptoUpsertIn) CryptoUpsertOut {
	mxdto := make([]models.ExchangeMaxDTO, len(in.Currency))
	mndto := make([]models.ExchangeMinDTO, len(in.Currency))

	i := 0
	for key, value := range in.Currency {
		mxdto[i].SetPair(key).SetMaxPrice(value.High)
		mndto[i].SetPair(key).SetMinPrice(value.Low)
		i++
	}

	err := e.storage.ExMax.Upsert(ctx, mxdto)
	if err != nil {
		e.logger.Error("Exchange: Upsert max err", zap.Error(err))
		return CryptoUpsertOut{ErrorCode: errors.CryptoMaxServiceUpsertErr}
	}

	err = e.storage.ExMin.Upsert(ctx, mndto)
	if err != nil {
		e.logger.Error("Exchange: Upsert min err", zap.Error(err))
		return CryptoUpsertOut{ErrorCode: errors.CryptoMinServiceUpsertErr}
	}

	return CryptoUpsertOut{
		Success: true,
	}
}

func (e *ExchangeService) GetPriceListMax(ctx context.Context) exchange.CryptoOut {
	list, err := e.storage.ExMax.GetList(ctx)
	if err != nil {
		e.logger.Error("Exchange: GetPriceListMax err", zap.Error(err))
		return exchange.CryptoOut{ErrorCode: errors.GetPriceListMaxErr}
	}

	out := make([]gateMod.CryptoPrice, len(list))

	for i := range list {
		out[i] = gateMod.CryptoPrice{
			ID:    list[i].GetID(),
			Pair:  list[i].GetPair(),
			Price: list[i].GetMaxPrice(),
		}
	}

	return exchange.CryptoOut{Currency: out}
}

func (e *ExchangeService) GetPriceListMin(ctx context.Context) exchange.CryptoOut {
	list, err := e.storage.ExMin.GetList(ctx)
	if err != nil {
		e.logger.Error("Exchange: GetPriceListMin err", zap.Error(err))
		return exchange.CryptoOut{ErrorCode: errors.GetPriceListMinErr}
	}

	out := make([]gateMod.CryptoPrice, len(list))

	for i := range list {
		out[i] = gateMod.CryptoPrice{
			ID:    list[i].GetID(),
			Pair:  list[i].GetPair(),
			Price: list[i].GetMinPrice(),
		}
	}

	return exchange.CryptoOut{Currency: out}
}

func (e *ExchangeService) GetPriceListAvg(ctx context.Context) exchange.CryptoOut {
	mxList, err := e.storage.ExMax.GetList(ctx)
	if err != nil {
		e.logger.Error("Exchange: GetPriceListAvg err", zap.Error(err))
		return exchange.CryptoOut{ErrorCode: errors.GetPriceListAvgErr}
	}

	mnList, err := e.storage.ExMin.GetList(ctx)
	if err != nil {
		e.logger.Error("Exchange: GetPriceListAvg err", zap.Error(err))
		return exchange.CryptoOut{ErrorCode: errors.GetPriceListAvgErr}
	}

	out := make([]gateMod.CryptoPrice, len(mxList))

	for i := range out {
		out[i] = gateMod.CryptoPrice{
			ID:    mxList[i].GetID(),
			Pair:  mxList[i].GetPair(),
			Price: (mxList[i].GetMaxPrice() + mnList[i].GetMinPrice()) / 2,
		}
	}

	return exchange.CryptoOut{Currency: out}
}
