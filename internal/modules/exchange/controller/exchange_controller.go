package controller

import (
	"net/http"

	"gitlab.com/timofeev.pavel.art/rpc_project-gateway_api/pkg/message/exchange_mess"

	"gitlab.com/timofeev.pavel.art/rpc_project-exchange_api/internal/infrastructure/errors"

	"github.com/ptflp/godecoder"
	"gitlab.com/timofeev.pavel.art/rpc_project-exchange_api/internal/infrastructure/component"
	"gitlab.com/timofeev.pavel.art/rpc_project-exchange_api/internal/infrastructure/responder"
	"gitlab.com/timofeev.pavel.art/rpc_project-exchange_api/internal/modules/exchange/service"
)

type Exchanger interface {
	Ticker(w http.ResponseWriter, r *http.Request)
	GetMaxList(w http.ResponseWriter, r *http.Request)
	GetAvgList(w http.ResponseWriter, r *http.Request)
	GetMinList(w http.ResponseWriter, r *http.Request)
}

type Exchange struct {
	service service.Exchanger
	responder.Responder
	godecoder.Decoder
}

func NewExchange(service service.Exchanger, components *component.Components) Exchanger {
	return &Exchange{service: service, Responder: components.Responder, Decoder: components.Decoder}
}

func (e *Exchange) Ticker(w http.ResponseWriter, r *http.Request) {
}

func (e *Exchange) GetMaxList(w http.ResponseWriter, r *http.Request) {
	out := e.service.GetPriceListMax(r.Context())

	if out.ErrorCode != errors.NoError {
		e.OutputJSON(w, exchange_mess.ListResponse{
			Success:   false,
			ErrorCode: out.ErrorCode,
			Data:      exchange_mess.Data{Message: "retrieving max list error"},
		})
		return
	}

	e.OutputJSON(w, exchange_mess.ListResponse{
		Success:   true,
		ErrorCode: out.ErrorCode,
		Data: exchange_mess.Data{
			List: out.Currency,
		},
	})
}

func (e *Exchange) GetAvgList(w http.ResponseWriter, r *http.Request) {
	out := e.service.GetPriceListAvg(r.Context())

	if out.ErrorCode != errors.NoError {
		e.OutputJSON(w, exchange_mess.ListResponse{
			Success:   false,
			ErrorCode: out.ErrorCode,
			Data:      exchange_mess.Data{Message: "retrieving avg list error"},
		})
		return
	}

	e.OutputJSON(w, exchange_mess.ListResponse{
		Success:   true,
		ErrorCode: out.ErrorCode,
		Data: exchange_mess.Data{
			List: out.Currency,
		},
	})
}

func (e *Exchange) GetMinList(w http.ResponseWriter, r *http.Request) {
	out := e.service.GetPriceListMin(r.Context())

	if out.ErrorCode != errors.NoError {
		e.OutputJSON(w, exchange_mess.ListResponse{
			Success:   false,
			ErrorCode: out.ErrorCode,
			Data:      exchange_mess.Data{Message: "retrieving min list error"},
		})
		return
	}

	e.OutputJSON(w, exchange_mess.ListResponse{
		Success:   true,
		ErrorCode: out.ErrorCode,
		Data: exchange_mess.Data{
			List: out.Currency,
		},
	})
}
