package storage

import (
	"context"

	"gitlab.com/timofeev.pavel.art/rpc_project-exchange_api/internal/models"
)

type ExchangerMax interface {
	Upsert(ctx context.Context, e []models.ExchangeMaxDTO) error
	GetList(ctx context.Context) ([]models.ExchangeMaxDTO, error)
}

type ExchangerMin interface {
	Upsert(ctx context.Context, e []models.ExchangeMinDTO) error
	GetList(ctx context.Context) ([]models.ExchangeMinDTO, error)
}
