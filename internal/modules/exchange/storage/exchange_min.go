package storage

import (
	"context"

	"gitlab.com/timofeev.pavel.art/rpc_project-exchange_api/internal/db/adapter"
	"gitlab.com/timofeev.pavel.art/rpc_project-exchange_api/internal/infrastructure/db/scanner"
	"gitlab.com/timofeev.pavel.art/rpc_project-exchange_api/internal/models"
)

type ExchangeMinStorage struct {
	adapter *adapter.SQLAdapter
}

func NewExchangeMin(sqlAdapter *adapter.SQLAdapter) *ExchangeMinStorage {
	return &ExchangeMinStorage{adapter: sqlAdapter}
}

func (e *ExchangeMinStorage) Upsert(ctx context.Context, m []models.ExchangeMinDTO) error {
	in := make([]scanner.Tabler, len(m))
	for i, item := range m {
		copyItem := item
		in[i] = &copyItem
	}

	err := e.adapter.Upsert(ctx, in)

	return err
}

func (e *ExchangeMinStorage) GetList(ctx context.Context) ([]models.ExchangeMinDTO, error) {
	var out []models.ExchangeMinDTO
	err := e.adapter.List(ctx, &out, "exchange_min", adapter.Condition{
		Order: []*adapter.Order{
			{
				Field: "pair",
				Asc:   true,
			},
		},
	})
	if err != nil {
		return nil, err
	}

	return out, nil
}
