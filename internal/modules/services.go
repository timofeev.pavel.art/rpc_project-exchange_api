package modules

import (
	"gitlab.com/timofeev.pavel.art/rpc_project-exchange_api/internal/infrastructure/component"
	"gitlab.com/timofeev.pavel.art/rpc_project-exchange_api/internal/modules/exchange/service"
	"gitlab.com/timofeev.pavel.art/rpc_project-exchange_api/internal/storages"
)

type Services struct {
	Exchange service.Exchanger
}

func NewServices(storages *storages.Storages, components *component.Components) *Services {
	return &Services{
		Exchange: service.NewExchangeService(*storages, components.Logger),
	}
}
