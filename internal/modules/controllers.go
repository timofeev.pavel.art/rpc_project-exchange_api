package modules

import (
	"gitlab.com/timofeev.pavel.art/rpc_project-exchange_api/internal/infrastructure/component"
	"gitlab.com/timofeev.pavel.art/rpc_project-exchange_api/internal/modules/exchange/controller"
)

type Controllers struct {
	Exchange controller.Exchanger
}

func NewControllers(services *Services, components *component.Components) *Controllers {
	return &Controllers{
		Exchange: controller.NewExchange(services.Exchange, components),
	}
}
