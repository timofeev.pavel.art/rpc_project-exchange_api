package models

import (
	"time"

	"gitlab.com/timofeev.pavel.art/rpc_project-exchange_api/internal/infrastructure/db/types"
)

//go:generate easytags $GOFILE json,db,db_ops,db_type,db_default,db_index

type ExchangeMinDTO struct {
	ID        int            `json:"id" db:"id" db_type:"BIGSERIAL primary key" db_default:"not null"`
	Pair      string         `json:"pair" db:"pair" db_ops:"create,conflict" db_type:"varchar(50)" db_default:"not null" db_index:"index,unique"`
	MinPrice  float64        `json:"min_price" db:"min_price" db_ops:"create,upsert" db_type:"numeric" db_default:"default null"`
	CreatedAt time.Time      `json:"created_at" db:"created_at" db_type:"timestamp" db_default:"default (now()) not null" db_index:"index"`
	UpdatedAt time.Time      `json:"updated_at" db:"updated_at" db_ops:"upsert" db_type:"timestamp" db_default:"default (now()) not null" db_index:"index"`
	DeletedAt types.NullTime `json:"deleted_at" db:"deleted_at" db_type:"timestamp" db_default:"default null" db_index:"index"`
}

func (e *ExchangeMinDTO) TableName() string {
	return "exchange_min"
}

func (e *ExchangeMinDTO) OnCreate() []string {
	return []string{}
}

func (e *ExchangeMinDTO) SetID(id int) *ExchangeMinDTO {
	e.ID = id
	return e
}

func (e *ExchangeMinDTO) GetID() int {
	return e.ID
}

func (e *ExchangeMinDTO) SetPair(pair string) *ExchangeMinDTO {
	e.Pair = pair
	return e
}

func (e *ExchangeMinDTO) GetPair() string {
	return e.Pair
}

func (e *ExchangeMinDTO) SetMinPrice(mxPrice float64) *ExchangeMinDTO {
	e.MinPrice = mxPrice
	return e
}

func (e *ExchangeMinDTO) GetMinPrice() float64 {
	return e.MinPrice
}

func (e *ExchangeMinDTO) SetCreatedAt(createdAt time.Time) *ExchangeMinDTO {
	e.CreatedAt = createdAt
	return e
}

func (e *ExchangeMinDTO) GetCreatedAt() time.Time {
	return e.CreatedAt
}

func (e *ExchangeMinDTO) SetUpdatedAt(updatedAt time.Time) *ExchangeMinDTO {
	e.UpdatedAt = updatedAt
	return e
}

func (e *ExchangeMinDTO) GetUpdatedAt() time.Time {
	return e.UpdatedAt
}

func (e *ExchangeMinDTO) SetDeletedAt(deletedAt time.Time) *ExchangeMinDTO {
	e.DeletedAt.Time.Time = deletedAt
	e.DeletedAt.Time.Valid = true
	return e
}

func (e *ExchangeMinDTO) GetDeletedAt() time.Time {
	return e.DeletedAt.Time.Time
}
