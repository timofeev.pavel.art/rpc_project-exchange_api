package models

import (
	"time"

	"gitlab.com/timofeev.pavel.art/rpc_project-exchange_api/internal/infrastructure/db/types"
)

//go:generate easytags $GOFILE json,db,db_ops,db_type,db_default,db_index

type ExchangeMaxDTO struct {
	ID        int            `json:"id" db:"id" db_type:"BIGSERIAL primary key" db_default:"not null"`
	Pair      string         `json:"pair" db:"pair" db_ops:"create,conflict" db_type:"varchar(50)" db_default:"not null" db_index:"index,unique"`
	MaxPrice  float64        `json:"max_price" db:"max_price" db_ops:"create,upsert" db_type:"numeric" db_default:"default null"`
	CreatedAt time.Time      `json:"created_at" db:"created_at" db_type:"timestamp" db_default:"default (now()) not null" db_index:"index"`
	UpdatedAt time.Time      `json:"updated_at" db:"updated_at" db_ops:"upsert" db_type:"timestamp" db_default:"default (now()) not null" db_index:"index"`
	DeletedAt types.NullTime `json:"deleted_at" db:"deleted_at" db_type:"timestamp" db_default:"default null" db_index:"index"`
}

func (e *ExchangeMaxDTO) TableName() string {
	return "exchange_max"
}

func (e *ExchangeMaxDTO) OnCreate() []string {
	return []string{}
}

func (e *ExchangeMaxDTO) SetID(id int) *ExchangeMaxDTO {
	e.ID = id
	return e
}

func (e *ExchangeMaxDTO) GetID() int {
	return e.ID
}

func (e *ExchangeMaxDTO) SetPair(pair string) *ExchangeMaxDTO {
	e.Pair = pair
	return e
}

func (e *ExchangeMaxDTO) GetPair() string {
	return e.Pair
}

func (e *ExchangeMaxDTO) SetMaxPrice(mxPrice float64) *ExchangeMaxDTO {
	e.MaxPrice = mxPrice
	return e
}

func (e *ExchangeMaxDTO) GetMaxPrice() float64 {
	return e.MaxPrice
}

func (e *ExchangeMaxDTO) SetCreatedAt(createdAt time.Time) *ExchangeMaxDTO {
	e.CreatedAt = createdAt
	return e
}

func (e *ExchangeMaxDTO) GetCreatedAt() time.Time {
	return e.CreatedAt
}

func (e *ExchangeMaxDTO) SetUpdatedAt(updatedAt time.Time) *ExchangeMaxDTO {
	e.UpdatedAt = updatedAt
	return e
}

func (e *ExchangeMaxDTO) GetUpdatedAt() time.Time {
	return e.UpdatedAt
}

func (e *ExchangeMaxDTO) SetDeletedAt(deletedAt time.Time) *ExchangeMaxDTO {
	e.DeletedAt.Time.Time = deletedAt
	e.DeletedAt.Time.Valid = true
	return e
}

func (e *ExchangeMaxDTO) GetDeletedAt() time.Time {
	return e.DeletedAt.Time.Time
}
