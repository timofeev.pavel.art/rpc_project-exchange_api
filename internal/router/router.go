package router

import (
	"net/http"

	"gitlab.com/timofeev.pavel.art/rpc_project-exchange_api/internal/infrastructure/middleware"

	"github.com/go-chi/chi/v5"
	"gitlab.com/timofeev.pavel.art/rpc_project-exchange_api/internal/infrastructure/component"
	"gitlab.com/timofeev.pavel.art/rpc_project-exchange_api/internal/modules"
)

func NewApiRouter(controllers *modules.Controllers, components *component.Components) http.Handler {
	r := chi.NewRouter()

	r.Route("/api", func(r chi.Router) {
		r.Route("/1", func(r chi.Router) {
			authCheck := middleware.NewTokenManager(components.Responder, components.TokenManager)
			r.Route("/exchange", func(r chi.Router) {
				exchangeController := controllers.Exchange
				r.Route("/max_list", func(r chi.Router) {
					r.Use(authCheck.CheckStrict)
					r.Get("/", exchangeController.GetMaxList)
				})
				r.Route("/avg_list", func(r chi.Router) {
					r.Use(authCheck.CheckStrict)
					r.Get("/", exchangeController.GetAvgList)
				})
				r.Route("/min_list", func(r chi.Router) {
					r.Use(authCheck.CheckStrict)
					r.Get("/", exchangeController.GetMinList)
				})
			})
		})
	})

	return r
}
