package docs

import (
	"gitlab.com/timofeev.pavel.art/rpc_project-gateway_api/pkg/models"
)

// swagger:route GET /api/1/exchange/max_list exchange maxExchangeRequest
// Получение списка максимальных цен
// security:
//	- Bearer: []
// responses:
//	200: maxExchangeResponse

//swagger:response maxExchangeResponse
type maxExchangeResponse struct {
	// in:body
	Body []models.CryptoPrice
}

// swagger:route GET /api/1/exchange/avg_list exchange avgExchangeRequest
// Получение списка средних цен
// security:
//	- Bearer: []
// responses:
//	200: avgExchangeResponse

//swagger:response avgExchangeResponse
type avgExchangeResponse struct {
	// in:body
	Body []models.CryptoPrice
}

// swagger:route GET /api/1/exchange/min_list exchange minExchangeRequest
// Получение списка минимальных цен
// security:
//	- Bearer: []
// responses:
//	200: minExchangeResponse

//swagger:response minExchangeResponse
type minExchangeResponse struct {
	// in:body
	Body []models.CryptoPrice
}
