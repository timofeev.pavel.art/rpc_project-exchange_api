package storages

import (
	"gitlab.com/timofeev.pavel.art/rpc_project-exchange_api/internal/db/adapter"
	"gitlab.com/timofeev.pavel.art/rpc_project-exchange_api/internal/modules/exchange/storage"
)

type Storages struct {
	ExMax storage.ExchangerMax
	ExMin storage.ExchangerMin
}

func NewStorages(sqlAdapter *adapter.SQLAdapter) *Storages {
	return &Storages{
		ExMax: storage.NewExchangeMax(sqlAdapter),
		ExMin: storage.NewExchangeMin(sqlAdapter),
	}
}
