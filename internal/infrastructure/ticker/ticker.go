package ticker

import (
	"context"
	"fmt"
	"io"
	"net/http"
	"strings"
	"time"

	not "gitlab.com/timofeev.pavel.art/rpc_project-exchange_api/internal/infrastructure/service"

	"gitlab.com/timofeev.pavel.art/rpc_project-exchange_api/internal/models"

	"go.uber.org/zap"

	"gitlab.com/timofeev.pavel.art/rpc_project-exchange_api/internal/modules/exchange/service"
)

const (
	reqTime         = time.Second * 10
	differencePlus  = 5
	differenceMinus = -5
)

type PriceTicker struct {
	service  service.Exchanger
	notifier not.Notifier
	logger   *zap.Logger
}

func NewPriceTicker(service service.Exchanger, notifier not.Notifier, logger *zap.Logger) *PriceTicker {
	return &PriceTicker{service: service, notifier: notifier, logger: logger}
}

func (p *PriceTicker) Serve(ctx context.Context) error {
	var (
		err   error
		res   models.CryptoMap
		check models.CryptoMap
	)
	chErr := make(chan error)

	err = p.notifier.Push(not.PushIn{
		Pair:    "test",
		Message: "test",
		Price:   0,
	})
	if err != nil {
		p.logger.Error("Send notification err", zap.Error(err))
	}

	check, err = req()
	if err != nil {
		p.logger.Info("req response err", zap.Error(err))
		return err
	}

	ticker := time.NewTicker(reqTime)
	go func() {
		p.logger.Info("worker started")
		for {
			select {
			case <-ctx.Done():
				p.logger.Info("worker: stopping ticker")
				return
			case <-ticker.C:
				res, err = req()
				if err != nil {
					p.logger.Error("worker error", zap.Error(err))
					chErr <- err
					return
				}

				p.service.Upsert(ctx, service.CryptoUpsertIn{Currency: res})

				for k, v := range res {
					min := v.Low - check[k].Low
					max := v.High - check[k].High
					if min >= differencePlus || min <= differenceMinus {
						err = p.notifier.Push(not.PushIn{
							Pair:    k,
							Message: "Новая минимальная цена",
							Price:   v.Low,
						})
						if err != nil {
							p.logger.Error("Send notification err", zap.Error(err))
						}
						update := models.Crypto{
							High: check[k].High,
							Low:  v.Low,
						}
						check[k] = update
					}

					if max >= differencePlus || max <= differenceMinus {
						err = p.notifier.Push(not.PushIn{
							Pair:    k,
							Message: "Новая максимальная цена",
							Price:   v.High,
						})
						if err != nil {
							p.logger.Error("Send notification err", zap.Error(err))
						}
						update := models.Crypto{
							High: v.High,
							Low:  check[k].Low,
						}
						check[k] = update
					}
				}
			}
		}
	}()

	select {
	case <-chErr:
		return err
	case <-ctx.Done():
	}

	return err
}

func req() (models.CryptoMap, error) {
	url := "https://api.exmo.com/v1.1/ticker"
	method := "POST"

	payload := strings.NewReader("")

	client := &http.Client{}
	req, err := http.NewRequest(method, url, payload)

	if err != nil {
		fmt.Println(err)
		return nil, err
	}
	req.Header.Add("Content-Type", "application/x-www-form-urlencoded")

	res, err := client.Do(req)
	if err != nil {
		fmt.Println(err)
		return nil, err
	}
	defer res.Body.Close()

	body, err := io.ReadAll(res.Body)
	if err != nil {
		fmt.Println(err)
		return nil, err
	}

	result, err := models.UnmarshalCrypto(body)
	if err != nil {
		fmt.Println(err)
		return nil, err
	}

	return result, nil
}
