package exchange

import (
	"context"

	"gitlab.com/timofeev.pavel.art/rpc_project-exchange_api/internal/modules/exchange/service"
	pb "gitlab.com/timofeev.pavel.art/rpc_project-gateway_api/pkg/proto/exchange_grpc"
)

type ExchangeServiceGRPC struct {
	exchangeService service.Exchanger
	pb.UnimplementedExchangerServer
}

func NewExchangeServiceGRPC(exchangeService service.Exchanger) *ExchangeServiceGRPC {
	return &ExchangeServiceGRPC{exchangeService: exchangeService}
}

func (e *ExchangeServiceGRPC) GetPriceListMax(ctx context.Context, request *pb.CryptoRequest) (*pb.CryptoResponse, error) {
	out := e.exchangeService.GetPriceListMax(ctx)

	cur := make([]*pb.Currency, len(out.Currency))

	for i, item := range out.Currency {
		cur[i] = &pb.Currency{
			ID:    int64(item.ID),
			Pair:  item.Pair,
			Price: item.Price,
		}
	}

	return &pb.CryptoResponse{
		ErrorCode: int64(out.ErrorCode),
		Currency:  cur,
	}, nil
}

func (e *ExchangeServiceGRPC) GetPriceListAvg(ctx context.Context, request *pb.CryptoRequest) (*pb.CryptoResponse, error) {
	out := e.exchangeService.GetPriceListAvg(ctx)

	cur := make([]*pb.Currency, len(out.Currency))

	for i, item := range out.Currency {
		cur[i] = &pb.Currency{
			ID:    int64(item.ID),
			Pair:  item.Pair,
			Price: item.Price,
		}
	}

	return &pb.CryptoResponse{
		ErrorCode: int64(out.ErrorCode),
		Currency:  cur,
	}, nil
}

func (e *ExchangeServiceGRPC) GetPriceListMin(ctx context.Context, request *pb.CryptoRequest) (*pb.CryptoResponse, error) {
	out := e.exchangeService.GetPriceListMin(ctx)

	cur := make([]*pb.Currency, len(out.Currency))

	for i, item := range out.Currency {
		cur[i] = &pb.Currency{
			ID:    int64(item.ID),
			Pair:  item.Pair,
			Price: item.Price,
		}
	}

	return &pb.CryptoResponse{
		ErrorCode: int64(out.ErrorCode),
		Currency:  cur,
	}, nil
}
