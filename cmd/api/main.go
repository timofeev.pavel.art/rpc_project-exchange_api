package main

import (
	"os"

	"github.com/joho/godotenv"
	"gitlab.com/timofeev.pavel.art/rpc_project-exchange_api/config"
	"gitlab.com/timofeev.pavel.art/rpc_project-exchange_api/internal/infrastructure/logs"
	"gitlab.com/timofeev.pavel.art/rpc_project-exchange_api/run"
)

func main() {
	// Загружаем переменные окружения из файла .env
	err := godotenv.Load()
	// Создаем конфигурацию приложения
	conf := config.NewAppConf()
	// Создаем логгер
	logger := logs.NewLogger(conf, os.Stdout)
	if err != nil {
		logger.Fatal("error loading .env file")
	}
	// Инициализируем конфигурацию приложения с логгером
	conf.Init(logger)
	// Создаем инстанс приложения
	app := run.NewApp(conf, logger)

	exitCode := app.
		// Инициализируем приложение
		Bootstrap().
		// Запускаем приложение
		Run()
	os.Exit(exitCode)
}
